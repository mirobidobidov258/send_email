package main

import (
	"database/sql"
	"time"

	"gitlab.com/mirobidobidov258/send_email/crone_algorithms/config"
	"gitlab.com/mirobidobidov258/send_email/crone_algorithms/email"
	"gitlab.com/mirobidobidov258/send_email/crone_algorithms/pkg/logger"
	"gitlab.com/mirobidobidov258/send_email/crone_algorithms/pkg/models"
	postgres "gitlab.com/mirobidobidov258/send_email/crone_algorithms/storage/postgres"
)

type CronJob struct {
	Cfg       *config.Config
	Postgres  *postgres.Postgres
	EmailSend *email.EmailSend
	Logger    logger.Logger
}

func main() {
	
	cfg := config.Load()
	log := logger.New(cfg.LogLevel, "crone_algorithms")
	defer logger.Cleanup(log)

	emailSend := email.NewEmailSend(cfg, log)
	db, err := postgres.NewPostgres(*cfg)
	if err != nil {
		panic(err)
	}
	defer db.Db.Close()

	cronJob := &CronJob{
		Cfg: cfg,
		Postgres: db,
		EmailSend: emailSend,
		Logger: log,
	}
	cronJob.Logger.Info(`Cron Job is working: `)

	for {
		scheduledMessage, err := cronJob.Postgres.GetPlannedMessage()
		if err != sql.ErrNoRows && err != nil {
			cronJob.Logger.Error(`Error while getting from database`, logger.Error(err))
		}

		for _, msg := range scheduledMessage {
			err = cronJob.EmailSend.SendingEmailToFollowers(
				&models.SendingEmailConfig{
					Email: msg.SendEmail,
					Password: msg.EmailPassword,
				},
				&models.SendingNewToFollowerReq{
					To: []*models.Follower{
						&msg.To,
					},
					News: msg.News,
				},)

			if err != nil {
				cronJob.Logger.Error(`Error sending emails`, logger.Error(err))
			}
			
			cronJob.Logger.Info(`Messages Sending Success to :` + msg.To.FirstName)
		}
		time.Sleep(time.Minute * 1)
	}

}
