package postgres

import (
	"fmt"
	"github.com/google/uuid"

	"github.com/jmoiron/sqlx"
	"gitlab.com/mirobidobidov258/send_email/crone_algorithms/config"
	"gitlab.com/mirobidobidov258/send_email/crone_algorithms/pkg/models"
)

type Postgres struct {
	Db *sqlx.DB
}

func NewPostgres(cfg config.Config) (*Postgres, error) {
	
	psqlString := fmt.Sprintf(`host=%s port=%s user=%s password=%s dbname=%s sslmode=disable`,
	cfg.PostgreHost,
	cfg.PostgrePort,
	cfg.PostgreUser,
	cfg.PostgrePassword,
	cfg.PostgreDatabase,
)

	conn, err := sqlx.Connect(`postgres`, psqlString)
	if err != nil {
		return &Postgres{}, err
	}

return &Postgres{Db: conn}, nil

}

func (p *Postgres) WriteMessagesToDB(req *models.SendPlanedEmailReq) error {
	newsId := uuid.New().String()
	err := p.Db.QueryRow(`INSERT INTO news (
		id,
		content, 
		send_email,
		email_password
	) values($1, $2, $3, $4)`,
		newsId,
		req.News,
		req.SendEmail,
		req.EmailPassword).Err()
	if err != nil {
		return err
	}

	for _, e := range req.To {
		err = p.Db.QueryRow(`INSERT INTO messages (
			id, 
			news_id,
			first_name,
			last_name,
			email,
			minutes_after)
			values($1, $2, $3, $4, $5, $6)`, 
		uuid.New().String(),
			newsId,
			e.FirstName,
			e.LastName,
			e.Email,
			req.MinutesAfter,
	).Err()
		if err != nil {
			return err
		}
	}

	return nil
}

func (p *Postgres) GetPlannedMessage() ([]*models.GetPlannedMessage, error) {
	responce := []*models.GetPlannedMessage{}
	rows, err := p.Db.Query(`
		SELECT
		news.content,
		news.send_email,
		news.email_password,
		messages.id,
		messages.first_name,
		messages.last_name,
		messages.email
		FROM messages INNER JOIN news ON news.id=messages.news_id
		WHERE EXTRACT(EPOCH FROM (CURRENT_TIMESTAMP - scheduled_at) >= (messages.minutes_after * 60))
	`,
)
	if err != nil {
		return responce, err
	}
	defer rows.Close()
	
	ids := []string{}
	for rows.Next()	{
		msg := &models.GetPlannedMessage{}
		msg.To = models.Follower{}
		id := ""
		err = rows.Scan(
			&msg.News,
			&msg.SendEmail,
			&msg.EmailPassword,
			&id,
			&msg.To.FirstName,
			&msg.To.LastName,
			&msg.To.Email,
		)
		if err != nil {
			return responce, err
		}

		ids = append(ids, id)
		responce = append(responce, msg)
	}

	err = p.DeleteMessageByID(ids)
	if err != nil {
		return responce, err
	}

return responce, err

}

func (p *Postgres) DeleteMessageByID(ids []string) error {
	for _, id := range ids {
		_, err := p.Db.Exec(`DELETE FROM messages where id = $1`, id)
		if err != nil {
			return err
		}
	}
	return nil
}