module gitlab.com/mirobidobidov258/send_email/crone_algorithms

go 1.19

require (
	github.com/google/uuid v1.3.0
	github.com/jmoiron/sqlx v1.3.5
	github.com/spf13/cast v1.5.0
	go.uber.org/zap v1.23.0
	golang.org/x/crypto v0.3.0
)

require (
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
)
