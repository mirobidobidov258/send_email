package email

import (
	"bytes"
	"html/template"
	"net/smtp"

	"gitlab.com/mirobidobidov258/send_email/crone_algorithms/config"
	"gitlab.com/mirobidobidov258/send_email/crone_algorithms/pkg/logger"
	"gitlab.com/mirobidobidov258/send_email/crone_algorithms/pkg/models"
)

type EmailSend struct {
	Cfg    *config.Config
	Logger logger.Logger
}

type Message struct {
	FirstName string
	LastName  string
	News      string
}

func NewEmailSend(cfg *config.Config, log logger.Logger) *EmailSend {
	return &EmailSend{
		Cfg:    cfg,
		Logger: log,
	}
}

func (e *EmailSend) SendingEmailToFollowers(ecfg *models.SendingEmailConfig, req *models.SendingNewToFollowerReq) error {
	for _, classic := range req.To {
		body := new(bytes.Buffer)
		t, err := template.ParseFiles(`./email/html_templates/news.html`)
		if err != nil {
			e.Logger.Error(`Error while parsing HTML template`, logger.Error(err))
			return err
		}

		message := &Message{
			FirstName: classic.FirstName,
			LastName: classic.LastName,
			News:      req.News,
		}
		t.Execute(body, message)
		mime := "MIME-version: 1.0;\n Content-Type: text/html; charset=\"UTF-8\"; \n\n"
		msg := []byte("Subject: Mailganer News\n" + mime + body.String())

		auth := smtp.PlainAuth("", ecfg.Email, ecfg.Password, "smtp.gmail.com")
		err = smtp.SendMail("smtp.gmail.com:587", auth, ecfg.Email, []string{classic.Email}, msg)
		if err != nil {
			e.Logger.Error(`Error while sending email`, logger.Error(err))
			return err
		}
	}
	return nil
}
