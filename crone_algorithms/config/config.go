package config

import (
	"os"

	"github.com/spf13/cast"
)

type Config struct {
	Environment     string
	LogLevel        string
	PostgreHost     string
	PostgrePort     string
	PostgreDatabase string
	PostgreUser     string
	PostgrePassword string
}

func Load() *Config {

	c := &Config{}

	c.Environment = cast.ToString(getOrReturnDefault(`ENVIRONMENT`, `develop`))
	c.LogLevel = cast.ToString(getOrReturnDefault(`LOG_LEVEL`, `debug`))

	c.PostgreHost = cast.ToString(getOrReturnDefault(`POSTGRES_HOST`, `localhost`))
	c.PostgrePort = cast.ToString(getOrReturnDefault(`POSTGRES_PORT`, `5432`))
	c.PostgreDatabase = cast.ToString(getOrReturnDefault(`POSTGRES_DATABASE`, `send_email`))
	c.PostgreUser = cast.ToString(getOrReturnDefault(`POSTGRES_USER`, `citizenfour`))
	c.PostgrePassword = cast.ToString(getOrReturnDefault(`POSTGRES_PASSWORD`, `12321`))

	return c
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}
	return defaultValue
}
