package etc

import "net/mail"

func ValidMailAddress(address string) (string, bool) {
	add, err := mail.ParseAddress(address)
	if err != nil {
		return "", false
	}

	return add.Address, true
}