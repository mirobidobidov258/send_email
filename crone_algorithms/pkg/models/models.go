package models

type Follower struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Email     string `json:"email"`
}

type GetPlannedMessage struct {
	News          string   `json:"news"`
	To            Follower `json:"followers"`
	SendEmail     string   `json:"send_email"`
	EmailPassword string   `json:"email_password"`
}

type SendingEmailConfig struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type SendingNewToFollowerReq struct {
	To   []*Follower `json:"followers"`
	News string      `json:"news"`
}

type SendPlanedEmailReq struct {
	News          string      `json:"news"`
	To            []*Follower `json:"followers"`
	MinutesAfter  int         `json:"minutes_after"`
	SendEmail     string      `json:"send_email"`
	EmailPassword string      `json:"email_password"`
}
