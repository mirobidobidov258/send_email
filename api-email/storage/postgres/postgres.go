package postgres

import (
	"fmt"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"gitlab.com/mirobidobidov258/send_email/api-email/api/models"
	"gitlab.com/mirobidobidov258/send_email/api-email/config"
)

type Postgres struct {
	Db *sqlx.DB
}

func NewPostgres(cfg config.Config) (*Postgres, error) {
	psqlString := fmt.Sprintf(`host=%s port=%s user=%s password=%s dbname=%s sslmode=disable`,
		cfg.PostgresHost,
		cfg.PostgresPort,
		cfg.PostgresUser,
		cfg.PostgresPassword,
		cfg.PostgresDatabase,
	)

	conn, err := sqlx.Connect(`postgres`, psqlString)
	if err != nil {
		return &Postgres{}, err
	}

	return &Postgres{Db: conn}, nil
}

func (p *Postgres) WriteMessagesToDB(req *models.SendScheduledEmailsReq) error {
	newsID := uuid.New().String()
	err := p.Db.QueryRow(`INSERT INTO news (
		id, 
		content,
		send_email,
		send_email_password) VALUES(
			$1, $2, $3, $4) `,
		newsID,
		req.News,
		req.SenderEmail,
		req.EmailPaassword).Err()
	if err != nil {
		return err
	}

	for _, e := range req.To {
		err := p.Db.QueryRow(`INSERT INTO messages(
			id,
			news_id,
			first_name,
			last_name,
			email,
			minutes_after
		) VALUES (
			$1, $2, $3, $4, $5, $6
		)`,
			uuid.New().String(),
			newsID,
			e.FirstName,
			e.LastName,
			e.Email,
			req.MinutsAfter).Err()
		if err != nil {
			return err
		}
	}
	return nil
}

func (p *Postgres) GetPlannedMessage() ([]*models.GetScheduledMessagesRes, error) {
	responce := []*models.GetScheduledMessagesRes{}
	rows, err := p.Db.Query(`
		SELECT
			n.content,
			n.send_email,
			n.send_email_password,
			m.id,
			m.first_name,
			m.last_name,
			m.email
		FROM 
			messages as m INNER JOIN news as n ON n.id=m.news_id
		WHERE
			EXTRACT(EPOCH FROM (CURRENT_TIMESTAMP - scheduled_at)) >= (m.minutes_after * 60)`,
	)
	if err != nil {
		return responce, err
	}
	defer rows.Close()
	ids := []string{}
	for rows.Next() {
		msg := &models.GetScheduledMessagesRes{}
		msg.To = &models.Subscriber{}
		id := ""
		err := rows.Scan(
			&msg.News,
			&msg.SenderEmail,
			&msg.EmailPaassword,
			id,
			&msg.To.FirstName,
			&msg.To.LastName,
			&msg.To.Email,
		)
		if err != nil {
			return responce, err
		}
		ids = append(ids, id)
		responce = append(responce, msg)
	}
	err = p.DeleteMessageByIds(ids)
	if err != nil {
		return []*models.GetScheduledMessagesRes{}, err
	}

	return responce, nil
}

func (p *Postgres) DeleteMessageByIds(ids []string) error {
	for _, id := range ids {
		_, err := p.Db.Exec(`DELETE FROM messages where id=$1`, id)
		if err != nil {
			return err
		}
	}
	return nil
}
