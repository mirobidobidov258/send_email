package redis

import (
	"github.com/gomodule/redigo/redis"
	"gitlab.com/mirobidobidov258/send_email/api-email/storage/repo"
)

type Redis struct {
	Rds *redis.Pool
}

func NewRedisRepo(rds *redis.Pool) repo.MemoryStorageI {
	return &Redis{
		Rds: rds,
	}
}

func (r *Redis) Exists(key string) (interface{}, error) {
	conn := r.Rds.Get()
	defer conn.Close()

	return conn.Do(`EXISTS`, key)

}

func (r *Redis) Get(key string) (interface{}, error) {
	conn := r.Rds.Get()
	defer conn.Close()

	return conn.Do(`GET`, key)

}

func (r *Redis) Set(key, value string) error {

	conn := r.Rds.Get()
	defer conn.Close()

	_, err := conn.Do(`SET`, key, value)
	return err

}

func (r *Redis) SetWithTTL(key, value string, seconds int) error {

	conn := r.Rds.Get()
	defer conn.Close()

	_, err := conn.Do(`SETEX`, key, value, seconds)
	return err

}
