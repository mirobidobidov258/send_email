package main

import (
	"github.com/casbin/casbin/v2"
	defaultrolemanager "github.com/casbin/casbin/v2/rbac/default-role-manager"
	"github.com/casbin/casbin/v2/util"
	"github.com/gomodule/redigo/redis"
	"gitlab.com/mirobidobidov258/send_email/api-email/api"
	"gitlab.com/mirobidobidov258/send_email/api-email/config"
	"gitlab.com/mirobidobidov258/send_email/api-email/email"
	"gitlab.com/mirobidobidov258/send_email/api-email/pkg/logger"
	"gitlab.com/mirobidobidov258/send_email/api-email/storage/postgres"
	r "gitlab.com/mirobidobidov258/send_email/api-email/storage/redis"
)

func main() {
	cfg := config.Load()
	log := logger.New(cfg.LogLevel, `api-email`)
	emailSend := email.SendNewEmail(cfg, log)

	casbinEnforcer, err := casbin.NewEnforcer(cfg.AuthConfigPath, cfg.CSVFilePath)
	if err != nil {
		log.Error(`Error with Casbin`, logger.Error(err))
		return
	}

	err = casbinEnforcer.LoadPolicy()
	if err != nil {
		log.Error(`Error error load policy`, logger.Error(err))
		return
	}

	casbinEnforcer.GetRoleManager().(*defaultrolemanager.RoleManager).AddMatchingFunc(`keyMatch`, util.KeyMatch)
	casbinEnforcer.GetRoleManager().(*defaultrolemanager.RoleManager).AddMatchingFunc(`keyMatch3`, util.KeyMatch3)

	pool := &redis.Pool{
		MaxIdle: 10,
		Dial: func() (redis.Conn, error) {
			return redis.Dial(`tcp`, cfg.RedisHost + ":" + cfg.RedisPort)
		},
	}
	
	postgres, err := postgres.NewPostgres(*cfg)
	if err != nil {
		log.Error(`Error with connecting to Postgres`, logger.Error(err))
		return
	}
	defer postgres.Db.Close()
	server := api.NewRouter(
		api.Option{
			Conf: cfg,
			Logger: log,
			EmailSend: emailSend,
			CasbinEnforcer: casbinEnforcer,
			Redis: r.NewRedisRepo(pool),
			Postgres: postgres,
		},
	)

	if err := server.Run(cfg.Host + ":" + cfg.Port); err != nil {
		log.Fatal("failed to run the server", logger.Error(err))
		return
	}

}
