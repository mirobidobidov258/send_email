package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/spf13/cast"
	"gitlab.com/mirobidobidov258/send_email/api-email/api/models"
	"gitlab.com/mirobidobidov258/send_email/api-email/pkg/etc"
	"gitlab.com/mirobidobidov258/send_email/api-email/pkg/logger"
)

// @Summary 	SendNewsToFollowers
// @Description	Through this api, news and informatin of all subscirbers will be sent
// @Tags		Email
// @Security	BearerAuth
// @Accept		json
// @Produce		json
// @Param		SendNewsToSupscribersReq 	body		models.SendNewsToSupscribersReq true "SendNewsToSupscribersReq"
// @Success		200							{object}	models.StatusInfo
// @Failure		500							{object}	models.StatusInfo
// @Failure		409							{object}	models.StatusInfo
// @Router		/email/tofollowers	[post]
func (h *Handler) SendNewToFollowers(c *gin.Context) {
	claims, err := GetClaims(*h, c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.StatusInfo{
			Message: "OOOooy something getclaims went wrong",
		})
		h.Log.Error(`Error while getting claims`, logger.Error(err))
		return
	}

	user := &models.SaveUserInRedis{}
	resRedis, err := h.Redis.Get(claims.Sub)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.StatusInfo{
			Message: "Connection with Redis Error",
		})
		h.Log.Error(`Error while getting from Redis`, logger.Error(err))
		return
	}

	resRedisString := cast.ToString(resRedis)

	err = json.Unmarshal([]byte(resRedisString), &user)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.StatusInfo{
			Message: "Something went wrong",
		})
		h.Log.Error(`Error while getting from JSON`, logger.Error(err))
		return
	}

	body := &models.SendNewsToSupscribersReq{}

	err = c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusConflict, &models.StatusInfo{
			Message: "Error Getting with BODY",
		})
		h.Log.Error(`Error while getting with BODY`, logger.Error(err))
		return
	}

	for _, e := range body.To {
		if _, ok := etc.ValidMailAddress(e.Email); !ok {
			c.JSON(http.StatusBadRequest, models.StatusInfo{
				Message: "Invalid email" + e.Email,
			})
			h.Log.Error(`Error with valid email`, logger.Error(err))
			return
		}
	}
	err = h.EmailSend.SendEmailToFollowers(&models.SendEmailConfig{
		Email:    user.Email,
		Passwrod: user.EmailPassword,
	}, body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.StatusInfo{
			Message: "Something went wrong",
		})
		h.Log.Error("Error while getting email with followers", logger.Error(err))
		return
	}

	c.JSON(http.StatusOK, models.StatusInfo{
		Message: "All emails send Successfully",
	})
}

// @Summary 		SendPlannedEmail
// @Description		Through this api, news and informatin of all subscirbers will be sent
// @Tags			Email
// @Security		BearerAuth
// @Accept			json
// @Produce			json
// @Param			SendScheduledEmailsApiReq	body		models.SendScheduledEmailsApiReq true "SendScheduledEmailsApiReq"
// @Success			200							{object}	models.StatusInfo
// @Failure			500							{object}	models.StatusInfo
// @Failure			409							{object}	models.StatusInfo
// @Router			/email/planemail [post]
func (h *Handler) SendPlannedEmail(c *gin.Context) {
	claims, err := GetClaims(*h, c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.StatusInfo{
			Message: "OOOooy something getclaims went wrong",
		})
		h.Log.Error(`Error while getting went wrong`, logger.Error(err))
		return
	}

	user := &models.SaveUserInRedis{}
	resRedis, err := h.Redis.Get(claims.Sub)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.StatusInfo{
			Message: "Connection with Redis Error",
		})
		h.Log.Error(`Error while getting from Redis`, logger.Error(err))
		return
	}

	resRedisString := cast.ToString(resRedis)

	err = json.Unmarshal([]byte(resRedisString), &user)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.StatusInfo{
			Message: "Something went wrong",
		})
		h.Log.Error(`Error while getting from JSON`, logger.Error(err))
		return
	}

	body := &models.SendScheduledEmailsApiReq{}

	err = c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusConflict, &models.StatusInfo{
			Message: "Error Getting with BODY",
		})
		h.Log.Error(`Error while getting with BODY`, logger.Error(err))
		return
	}

	for _, e := range body.To {
		if email, ok := etc.ValidMailAddress(e.Email); !ok {
			c.JSON(http.StatusConflict, models.StatusInfo{
				Message: "Invalid mail address" + email,
			})
			return
		}
	}

	message := &models.SendScheduledEmailsReq{
		News:           body.News,
		MinutsAfter:    body.MinutsAfter,
		SenderEmail:    user.Email,
		EmailPaassword: user.EmailPassword,
		To:             body.To,
	}

	err = h.Postgres.WriteMessagesToDB(message)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.StatusInfo{
			Message: "Oops something went wrong",
		})
		h.Log.Error("Error while writing to database", logger.Error(err))
		return
	}

	c.JSON(http.StatusOK, &models.StatusInfo{
		Message: "Send Email Succesfully",
	})
}
