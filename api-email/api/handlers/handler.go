package handlers

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/spf13/cast"
	"gitlab.com/mirobidobidov258/send_email/api-email/api/tokens"
	"gitlab.com/mirobidobidov258/send_email/api-email/config"
	"gitlab.com/mirobidobidov258/send_email/api-email/email"
	"gitlab.com/mirobidobidov258/send_email/api-email/pkg/logger"
	"gitlab.com/mirobidobidov258/send_email/api-email/storage/postgres"
	"gitlab.com/mirobidobidov258/send_email/api-email/storage/repo"
)

type Handler struct {
	Log        logger.Logger
	Cfg        *config.Config
	EmailSend  *email.EmailSend
	Redis      repo.MemoryStorageI
	JWTHandler tokens.JWTHandler
	Postgres   *postgres.Postgres
}

func GetClaims(h Handler, c *gin.Context) (*tokens.CustomClaims, error) {
	var (
		claims = tokens.CustomClaims{}
	)
	strToken := c.GetHeader(`Authorization`)

	token, err := jwt.Parse(strToken, func(t *jwt.Token) (interface{}, error) {return []byte(h.Cfg.SignInKey), nil})
	if err != nil {
		h.Log.Error(`invalid access token`)
		return nil, err
	}
	rawClaims := token.Claims.(jwt.MapClaims)

	claims.Sub = rawClaims["sub"].(string)
	claims.Exp = rawClaims["exp"].(float64)
	aud := cast.ToStringSlice(rawClaims["aud"])
	claims.Aud = aud
	claims.Role = rawClaims["role"].(string)
	claims.Sub = rawClaims["sub"].(string)
	claims.Token = token

	return &claims, nil

}