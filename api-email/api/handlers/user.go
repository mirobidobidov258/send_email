package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/spf13/cast"
	"gitlab.com/mirobidobidov258/send_email/api-email/api/models"
	"gitlab.com/mirobidobidov258/send_email/api-email/pkg/etc"
	"gitlab.com/mirobidobidov258/send_email/api-email/pkg/logger"
)

// @Summary 	Register
// @Description Through this api, news and informatin of all subscirbers will be sent
// @Tags		User
// @Accept		json
// @Produce		json
// @Param		User		body 		models.User true "User"
// @Success		200			{object} 	models.SaveUserInRedis
// @Failure		500 		{object} 	models.StatusInfo
// @Failure		409			{object} 	models.StatusInfo
// @Router		/user/register 			[post]
func (h *Handler) Register(c *gin.Context) {
	body := &models.User{}

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusConflict, &models.StatusInfo{
			Message: "Error Getting with BODY",
		})
		h.Log.Error(`Error while getting with BODY`, logger.Error(err))
		return
	}

	isUniqueEmail, err := h.Redis.Exists(body.Email)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.StatusInfo{
			Message: "Please check your data",
		})
		h.Log.Error("Error while checking email uniqeness", logger.Error(err))
		return
	}
	if cast.ToString(isUniqueEmail) == "1" {
		c.JSON(http.StatusConflict, models.StatusInfo{
			Message: "This email is already registered",
		})
		h.Log.Error(`Error while checking email unique`, logger.Error(err))
		return
	}

	body.Password, err = etc.HashPassword(body.Password)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.StatusInfo{
			Message: "Doesn`t Hash Password",
		})
		h.Log.Error("couldn`t hash the password", logger.Error(err))
		return
	}

	bodyWithId := models.SaveUserInRedis{
		Id:            uuid.New().String(),
		Name:          body.Name,
		Password:      body.Password,
		Email:         body.Email,
		EmailPassword: body.Password,
	}

	saveInRedisByte, err := json.Marshal(bodyWithId)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.StatusInfo{
			Message: "Something went wrong with MARSHAL",
		})
		h.Log.Error("Error while marshaling", logger.Error(err))
		return
	}

	err = h.Redis.Set(bodyWithId.Email, string(saveInRedisByte))
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.StatusInfo{
			Message: "Something went wrong with Redis SET",
		})
		h.Log.Error(`Error while Redis`, logger.Error(err))
		return
	}

	h.JWTHandler.Sub = bodyWithId.Email
	h.JWTHandler.Role = "authorized"
	h.JWTHandler.Aud = []string{"mailganer"}
	h.JWTHandler.SigninKey = h.Cfg.SignInKey
	h.JWTHandler.Log = h.Log
	tokens, err := h.JWTHandler.GenerateAuthJWT()
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.StatusInfo{
			Message: "Something went wrong with TOKEN",
		})
		h.Log.Error(`Error while Tokens`, logger.Error(err))
		return
	}
	accessToken := tokens[0]
	bodyWithId.AccessToken = accessToken

	c.JSON(http.StatusOK, bodyWithId)
}

// @Summary 	Login
// @Description Through this api, User can login
// @Tags 		User
// @Accept		json
// @Produce 	json
// @Param		email			query		string true "email"
// @Param		password		query		string true "password"
// @Success 	200 			{object}	models.SaveUserInRedis
// @Failure		500 			{object}	models.StatusInfo
// @Failure		409				{object}	models.StatusInfo
// @Router	/user/login	[get]
func (h *Handler) Login(c *gin.Context) {
	var (
		email    = c.Query("email")
		password = c.Query("password")
		body     = &models.SaveUserInRedis{}
	)

	resRedis, err := h.Redis.Get(email)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.StatusInfo{
			Message: "You haven`t connect to REDIS",
		})
		h.Log.Error(`Error while getting from REDIS`, logger.Error(err))
		return
	}

	resRedisString := cast.ToString(resRedis)

	err = json.Unmarshal([]byte(resRedisString), &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.StatusInfo{
			Message: "Ooops something with UNMARSHAL",
		})
		h.Log.Error(`Error while getting Unmarshal`, logger.Error(err))
		return
	}

	if !etc.CheckPasswordHash(password, body.Password) {
		c.JSON(http.StatusConflict, models.StatusInfo{
			Message: "Error is Password Hashing",
		})
		h.Log.Error(`Error while getting Hash Password`, logger.Error(err))
		return
	}

	h.JWTHandler.Sub = body.Email
	h.JWTHandler.Role = "authorized"
	h.JWTHandler.Aud = []string{"mailganer"}
	h.JWTHandler.SigninKey = h.Cfg.SignInKey
	h.JWTHandler.Log = h.Log
	tokens, err := h.JWTHandler.GenerateAuthJWT()
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.StatusInfo{
			Message: "Something went wrong with TOKEN",
		})
		h.Log.Error(`Error while Tokens`, logger.Error(err))
		return
	}
	accessToken := tokens[0]
	body.AccessToken = accessToken

	c.JSON(http.StatusOK, body)
}

// @Summary 		Profile
// @Description 	Through this api, User can login
// @Security		BearerAuth
// @Tags			User
// @Accept			json
// @Produce			json
// @Success			200 	{object}	models.SaveUserInRedis
// @Failure			500		{object}	models.StatusInfo
// @Failure			409		{object}	models.StatusInfo
// @Router		/user/profile	[get]	
func (h *Handler) Profile(c *gin.Context) {
	claims, err := GetClaims(*h, c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.StatusInfo{
			Message: "OOOooy something getclaims went wrong",
		})
		h.Log.Error(`Error while getting claims`, logger.Error(err))
		return
	}

	body := &models.SaveUserInRedis{}
	resRedis, err := h.Redis.Get(claims.Sub)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.StatusInfo{
			Message: "Connection with Redis Error",
		})
		h.Log.Error(`Error while getting from Redis`, logger.Error(err))
		return
	}

	resRedisString := cast.ToString(resRedis)

	err = json.Unmarshal([]byte(resRedisString), &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, &models.StatusInfo{
			Message: "Oops something went wrong",
		})
		h.Log.Error("Error while unmarshaling from json", logger.Error(err))
		return
	}

	c.JSON(http.StatusOK, body)
}
