package middleware

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/casbin/casbin/v2"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	token "gitlab.com/exam23/api-gateway/api/tokens"
	"gitlab.com/mirobidobidov258/send_email/api-email/api/models"
	"gitlab.com/mirobidobidov258/send_email/api-email/config"
)

type JwtRoleAuth struct {
	enforcer *casbin.Enforcer
	cnf 	config.Config
	jwtHandler 	token.JWTHandler
}

func NewAuth(enforce *casbin.Enforcer, 	jwtHandler token.JWTHandler, cfg config.Config) gin.HandlerFunc {

	a := &JwtRoleAuth{
		enforcer: enforce,
		cnf: cfg,
		jwtHandler: jwtHandler,
	}

	return func(ctx *gin.Context) {
		allow, err := a.CheckPermission(ctx.Request)
		fmt.Println(allow)
		if err != nil {
			v, _ := err.(*jwt.ValidationError)
			if v.Errors == jwt.ValidationErrorExpired {
				a.RequireRefresh(ctx)
			}else {
				a.RequirePermission(ctx)
			}
		}else if !allow {
			a.RequirePermission(ctx)
		}
	}
}

// GetRole gets role from Authorization header if there is a token then it is
// parsed and in role got from role claim. If there is no token then role is
// unauthorized
func (j *JwtRoleAuth) GetRole(r *http.Request) (string, error) {
	var (
		role string
		claims jwt.MapClaims
		err	error
	)

	jwtToken := r.Header.Get(`Authorization`)
	if jwtToken == "" {
		return `unauthorized`, err
	}else if strings.Contains(jwtToken, `Basic`) {
		return `unauthorized`, err
	}

	j.jwtHandler.Token = jwtToken
	claims, err = j.jwtHandler.ExtractClaims()
	if err != nil {
		return "", err
	}

	if claims["role"].(string) == "authorized" {
		role = "authorized"
	}else {
		role = "unknown"
	}

	return role, nil

}

// CheckPermission checks whether user is allowed to use certain endpoint
func (j *JwtRoleAuth) CheckPermission(r *http.Request) (bool, error) {
	user, err := j.GetRole(r)
	if err != nil {
		return false, err
	}
	fmt.Println(user)
	method := r.Method
	path := r.URL.Path


	allowed, err := j.enforcer.Enforce(user, path, method)
	if err != nil {
		panic(err)
	}

	return allowed, err

}

// RequirePermission aborts request with 403 status
func (j *JwtRoleAuth) RequirePermission(c *gin.Context) {
	c.AbortWithStatus(403)
}

// RequireRefresh aborts request with 401 status
func (j *JwtRoleAuth) RequireRefresh(c *gin.Context) {
	c.JSON(http.StatusUnauthorized, models.StatusInfo{
		Message: `Token has expired`,
	})
	c.AbortWithStatus(401)
}