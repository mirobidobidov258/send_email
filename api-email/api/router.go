package api

import (
	"github.com/casbin/casbin/v2"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	token "gitlab.com/exam23/api-gateway/api/tokens"
	"gitlab.com/mirobidobidov258/send_email/api-email/api/handlers"
	"gitlab.com/mirobidobidov258/send_email/api-email/api/middleware"
	"gitlab.com/mirobidobidov258/send_email/api-email/config"
	"gitlab.com/mirobidobidov258/send_email/api-email/email"
	"gitlab.com/mirobidobidov258/send_email/api-email/pkg/logger"
	"gitlab.com/mirobidobidov258/send_email/api-email/storage/postgres"
	"gitlab.com/mirobidobidov258/send_email/api-email/storage/repo"
)

type Option struct {
	Conf           *config.Config
	Logger         logger.Logger
	EmailSend      *email.EmailSend
	CasbinEnforcer *casbin.Enforcer
	Redis          repo.MemoryStorageI
	Postgres       *postgres.Postgres
}

// New ...
// @title 	SEND_EMAIL-TEST
// @version 1.0
// @description This test task server

// @contact.name Mirobid
// @contact.url	 https://t.me/mirobidmirsoatov
// @contact.email mirobidobidov258@gmail.com

// @host		localhost:9090

// @securityDefinition.apikey BearerAuth
// @in header
// @name Authorization
func NewRouter(opt Option) *gin.Engine {
	router := gin.New()

	router.Use(gin.Logger())
	router.Use(gin.Recovery())
	jwtHandler := token.JWTHandler{
		SigninKey: opt.Conf.SignInKey,
		Log:       opt.Logger,
	}

	router.Use(middleware.NewAuth(opt.CasbinEnforcer, jwtHandler, *opt.Conf))
	handler := handlers.Handler{
		Log:       opt.Logger,
		Cfg:       opt.Conf,
		EmailSend: opt.EmailSend,
		Redis:     opt.Redis,
		Postgres:  opt.Postgres,
	}

	router.POST("/email/tofollowers", handler.SendNewToFollowers)
	router.POST("/email/planemail", handler.SendPlannedEmail)

	router.GET("/user/register", handler.Register)
	router.GET("/user/login", handler.Login)
	router.GET("/user/profile", handler.Profile)

	url := ginSwagger.URL("swagger/doc.json")
	router.GET("swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))

	return router

}
