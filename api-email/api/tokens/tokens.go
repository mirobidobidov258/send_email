package tokens

import (
	"fmt"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"gitlab.com/mirobidobidov258/send_email/api-email/pkg/logger"
)

type JWTHandler struct {
	Sub       string
	Exp       string
	Iat       string
	Aud       []string
	Role      string
	SigninKey string
	Log       logger.Logger
	Token     string
}

type CustomClaims struct {
	*jwt.Token
	Sub  string   `json:"sub"`
	Exp  float64  `json:"exp"`
	Iat  float64  `json:"iat"`
	Aud  []string `json:"aud"`
	Role string   `json:"role"`
}

// GenerateAuthJWT ...
func (jwtHandler *JWTHandler) GenerateAuthJWT() ([]string, error) {
	var (
		accessToken  *jwt.Token
		refreshToken *jwt.Token
		claims       jwt.MapClaims
	)
	accessToken = jwt.New(jwt.SigningMethodHS256)
	refreshToken = jwt.New(jwt.SigningMethodHS256)

	claims = accessToken.Claims.(jwt.MapClaims)
	claims["sub"] = jwtHandler.Sub
	claims["exp"] = time.Now().Add(time.Hour * 500).Unix()
	claims["iat"] = time.Now().Unix()
	claims["role"] = jwtHandler.Role
	claims["aud"] = jwtHandler.Aud

	access, err := accessToken.SignedString([]byte(jwtHandler.SigninKey))
	if err != nil {
		jwtHandler.Log.Error(`error generating access token`, logger.Error(err))
		return []string{access, ""}, err
	}

	refresh, err := refreshToken.SignedString([]byte(jwtHandler.SigninKey))
	if err != nil {
		jwtHandler.Log.Error(`error generating refresh token`, logger.Error(err))
		return []string{refresh, ""}, err
	}

	return []string{access, refresh}, nil

}

//ExtraClaims ...

func (jwtHandler *JWTHandler) ExtractClaims() (jwt.MapClaims, error) {
	var (
		token *jwt.Token
		err   error
	)

	token, err = jwt.Parse(jwtHandler.Token, func(t *jwt.Token) (interface{}, error) {
		return []byte(jwtHandler.SigninKey), err
	})
	if err != nil {
		return nil, err
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !(ok && token.Valid)	{
		jwtHandler.Log.Error(`invalid jwt token`)
		return nil, err
	}

return claims, nil

}

// ExtraClaims extract claims from given token 
func ExtractClaim(tokenStr string, signInKey []byte) (jwt.MapClaims, error) {
	var (
		token *jwt.Token
		err error
	)
	fmt.Println(tokenStr)
	token, err = jwt.Parse(tokenStr, func(t *jwt.Token) (interface{}, error) {
		// check token signing method etc 
		return signInKey, err
	
	})
	if err != nil {
		return nil, err
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !(ok && token.Valid) {
		return nil, err
	}

return claims, nil

}

